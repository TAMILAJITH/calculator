import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-routing',
  templateUrl: './routing.component.html',
  styleUrls: ['./routing.component.css'],
})
export class RoutingComponent implements OnInit {
  constructor(private router: Router) {}
  public loopObject: check[] = [
    { id: 1, name: 'ajith', age: 2 },
    { id: 2, name: 'tamil', age: 21 },
    { id: 3, name: 'sujith', age: 22 },
    { id: 4, name: 'backia', age: 23 },
    { id: 5, name: 'arasan', age: 24 },
  ];
  ngOnInit(): void {}
  callId(value) {
    this.router.navigate(['/routing', value.id]);
  }
}

export class check {
  id?: number;
  name?: string;
  age?: number;
}
