import { Component, OnInit } from '@angular/core';
import {
  fromEvent,
  Observable,
  of,
  from,
  interval,
  timer,
  combineLatest,
  concat,
} from 'rxjs';
import { debounceTime, map, take } from 'rxjs/operators';

@Component({
  selector: 'app-check-rxjs',
  templateUrl: './check-rxjs.component.html',
  styleUrls: ['./check-rxjs.component.css'],
})
export class CheckRxjsComponent implements OnInit {
  myValue: any = 'Hi Helo Welcome';
  constructor() {
    const value = fromEvent(document, 'click');
    const myValue = value.pipe(debounceTime(1000));
    myValue.subscribe((x) => (this.globalValue = x));

    const fromValue = from([['Ajith', 'Sujith', 'Team']]);
    fromValue.subscribe((x) => (this.apiValue = x));

    const firstValue = from([['sujith', 'Ajith']]);
    const secondValue = from([['Backia', 'Arasan']]);
    const combineValue = combineLatest(firstValue, secondValue);
    combineValue.subscribe((x) => console.log(x));
    const concatValue = interval(2000).pipe(
      take(5),
      map((x) => x + 10)
    );
    const concatNextValue = interval(4000).pipe(
      take(5),
      map((x) => x + 2)
    );
    const concatSplitValue = concat(concatValue, concatNextValue);
    concatSplitValue.subscribe((x) => console.log(x));
  }
  public checkValue;
  public globalValue;
  public apiValue;

  ngOnInit() {
    let obs: Observable<any> = new Observable<any>();
    obs.subscribe((x) => (this.myValue = x));

    let checkEvent = document.getElementById('check');
    const eventFix = fromEvent(checkEvent, 'click').pipe();
    eventFix.subscribe(() => {
      alert('Its completely working');
    });
  }
}
