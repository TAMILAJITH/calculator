import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'numberSplitter',
})
export class NumberSplitterPipe implements PipeTransform {
  transform(value: string) {
    let numberValue = Number(value);
    let splitValue = numberValue.toLocaleString('en-IN');
    return splitValue;
    //   let dotSplitter: string[] = value.split('.');
    //   let splitValue: string[] = dotSplitter[0].split('');
    //   let ln: number = splitValue.length;

    //   if (ln >= 4) {
    //     splitValue.splice(ln - 3, 0, ',');
    //     ln++;
    //   }

    //   if (ln >= 7) {
    //     splitValue.splice(ln - 6, 0, ',');
    //     ln++;
    //   }
    //   if (ln >= 10) {
    //     splitValue.splice(ln - 9, 0, ',');
    //     ln++;
    //   }

    //   if (ln >= 13) {
    //     splitValue.splice(ln - 12, 0, ',');
    //     ln++;
    //   }
    //   if (dotSplitter[1]) {
    //     return splitValue.join('') + '.' + dotSplitter[1];
    //   } else {
    //     return splitValue.join('');
    //   }
    // }
  }
}
