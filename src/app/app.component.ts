/**
 * written by- Ajith
 * modified by-
 * reviewed by-Raja
 * created date-
 * modified date-
 */

import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  // this variable used for store history value
  public history: string = '';

  // this variable used for store current value
  public currentValue: string = '';

  // this variable used for store current answer
  public currentAnswer: string = '';

  // this variable used for apply conditionally view after calculate
  public hasAnswer: boolean = false;

  // this variable used for conditionally display (=) operator
  public showAnswer: boolean = false;

  /** This method used here for display value as per given input by user with conditionally */
  getNumber(currentOp: string) {
    if (this.currentValue.length == 0 && currentOp == '.') {
      this.currentValue += '0';
    }
    if (currentOp == '.') {
      let lastValue = this.currentValue[this.currentValue.length - 1];
      if (lastValue == '.') {
        return;
      }
    }

    if (this.history.length > 0) {
      if (this.showAnswer || (this.hasAnswer && this.isOperator(currentOp))) {
        this.history = this.currentAnswer + currentOp;
        this.currentAnswer = '';
        this.showAnswer = false;
        this.hasAnswer = false;
        return;
      }
    }

    if (
      this.history.length == 0 &&
      this.currentValue.length == 0 &&
      this.isOperator(currentOp)
    ) {
      this.history += '0' + currentOp;
    }

    if (this.isOperator(currentOp)) {
      this.history += this.currentValue;
      this.currentValue = '';
      this.calCulate(false);
      let lst = this.history[this.history.length - 1];
      if (this.isOperator(lst)) {
        if (this.isOperator(currentOp)) {
          if (lst == currentOp) {
            return;
          } else {
            let lastValue = this.history.substr(0, this.history.length - 1);
            this.history = lastValue + currentOp;
          }
        } else {
          this.history += currentOp;
        }
      } else {
        this.history += currentOp;
      }
    } else {
      this.currentValue += currentOp;
    }
  }
  /** In this method we clear all value and refresh for new entry */
  allClear() {
    this.history = '';
    this.currentValue = '';
    this.currentAnswer = '';
    this.hasAnswer = false;
    this.showAnswer = false;
  }
  /**This part used for separate operator value   */
  operator = ['+', '-', '*', '/'];
  isOperator(op: string) {
    return this.operator.some((x) => x == op);
  }
  /** This method here for clear last one value with conditionally  */
  splice() {
    if (this.currentValue.length == 0) {
      this.history = this.history.slice(0, -1);
      this.calCulate(false);
      this.hasAnswer = false;
      this.showAnswer = false;
    } else {
      this.currentValue = this.currentValue.slice(0, -1);
    }
  }

  /** This method used for clear last two value as per the decision of user */
  spliceTwoValue() {
    if (this.currentValue.length == 0) {
      this.history = this.history.slice(0, -2);
    } else {
      this.currentValue = this.history.slice(0, -2);
    }
  }

  /** This method here for
   * Divide given value by user one from two for calculate.
   * Display calculated value in html page.
   */
  calCulate(isFinal: boolean) {
    let lst = this.history[this.history.length - 1];
    this.currentAnswer = '';
    this.history += this.currentValue;
    let allValue = this.history
      .split(/(\+|\-|\/|\*)/)
      .filter((x) => x != '')
      .map((x) => x.trim());
    let ans = 0;

    if (allValue.length >= 3) {
      if (this.isOperator(allValue[0])) {
        ans = parseFloat(eval(allValue.shift() + allValue.shift()));
      } else {
        ans = parseFloat(allValue.shift());
      }
      ans = this.standardCalculator(ans, allValue);
    }

    if (
      this.history.length >= 3 &&
      this.isOperator(lst) &&
      this.currentValue.length > 0
    ) {
      this.showAnswer = isFinal;
    }
    if (
      isFinal &&
      this.history.length >= 3 &&
      this.isOperator(lst) &&
      this.currentValue.length == 0
    ) {
      let op = this.history[this.history.length - 1];
      let r = ans + '';
      let value = this.getCalculateValue(ans + '', op, r);

      this.history += ans;
      this.hasAnswer = isFinal;
      this.currentAnswer = value + '';
      return this.currentAnswer;
    }

    this.currentValue = '';
    this.currentAnswer = ans + '';
  }

  /** This method used for divide the input value two from three.
   * Because we need compulsory three value for calculate and
   * calculate continuously till last value calculated.
   */
  standardCalculator(ans: number, values: string[]) {
    if (values.length >= 2) {
      let op = values.shift();
      let r = values.shift();
      ans = this.getCalculateValue(ans + '', op, r);
      if (values.length > 0) {
        ans = this.standardCalculator(ans, values);
      }
    }
    return ans;
  }

  /** This method used for calculate values given by user.
   * In this class,this method play virtual function.
   */
  getCalculateValue(l: string, op: string, r: string) {
    let left: number = parseFloat(l);
    let right: number = parseFloat(r);
    switch (op) {
      case '+':
        return left + right;
      case '-':
        return left - right;
      case '*':
        return left * right;
      case '/':
        return left / right;
      default:
        return 0;
    }
  }
}
