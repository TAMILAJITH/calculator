import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, appRoutingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { StandardComponent } from './standard/standard.component';
import { NumberSplitterPipe } from './number-splitter.pipe';

@NgModule({
  declarations: [
    AppComponent,
    StandardComponent,
    NumberSplitterPipe,
    appRoutingComponent,
  ],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
