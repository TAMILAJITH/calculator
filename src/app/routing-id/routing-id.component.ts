import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Route } from '@angular/compiler/src/core';
@Component({
  selector: 'app-routing-id',
  templateUrl: './routing-id.component.html',
  styleUrls: ['./routing-id.component.css'],
})
export class RoutingIdComponent implements OnInit {
  constructor(private route: ActivatedRoute, private router: Router) {}
  public myValue;
  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(params.get('id'));
      this.myValue = id;
    });
  }
  goPrevious() {
    let previousId = this.myValue - 1;
    this.router.navigate(['/routing', previousId]);
  }
  goNext() {
    let nextId = this.myValue + 1;
    this.router.navigate(['/routing', nextId]);
  }
  goParent() {
    this.router.navigate(['/routing']);
  }
}
