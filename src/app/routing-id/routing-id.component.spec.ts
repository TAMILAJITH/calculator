import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoutingIdComponent } from './routing-id.component';

describe('RoutingIdComponent', () => {
  let component: RoutingIdComponent;
  let fixture: ComponentFixture<RoutingIdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoutingIdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoutingIdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
