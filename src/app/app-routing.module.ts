import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoutingComponent } from './routing/routing.component';
import { RoutingDetailComponent } from './routing-detail/routing-detail.component';
import { RoutingIdComponent } from './routing-id/routing-id.component';
import { CheckRxjsComponent } from './check-rxjs/check-rxjs.component';

const routes: Routes = [
  { path: 'Rxjs', component: CheckRxjsComponent },
  { path: 'routing', component: RoutingComponent },
  { path: 'routing-detail', component: RoutingDetailComponent },
  { path: 'routing/:id', component: RoutingIdComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
export const appRoutingComponent = [
  CheckRxjsComponent,
  RoutingComponent,
  RoutingDetailComponent,
  RoutingIdComponent,
];
